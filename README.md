INTRODUCTION

This is a Landing page for a PPE Manufacturer to generate order inquiries using HTML, CSS and Javascript. 
A shop now button is provied to the user which will redirect to the booking platform. Three links are given at the top for 
Full Body PPE kit, Face PPE kit and Hand PPE kit. The user can click on any one of them which will also redirect them to the 
booking platform. After filling up the form a confirmation alert will be generated.

TECHNOLOGIES
HTML
CSS
JavaScript
